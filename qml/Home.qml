// Main.qml
//
// This file is part of the UBports Welcome App.
//
// Copyright © 2017-2018 UBports https://ubports.com
//
// Maintained by Jan Jakob Sprinz (@NeoTheThird) <jan@ubports.com>
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.4
import Ubuntu.Components 1.3
import "modules"

Page {
    id: homePage
    title: i18n.tr("Welcome to %1").arg("UBports")

    header: DefaultHeader {
        flickable: scroll.flickableItem
    }

    property int yumiFrame: 0

    ScrollView {
        id: scroll
        anchors.fill: parent

        Column {
            id: homeColumn
            width: scroll.width
            spacing: colSpacing

            Image {
                id: yumi
                width: units.gu(25)
                height: width
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Image.AlignHCenter
                source: yumiFrame === 24 ? Qt.resolvedUrl("../assets/logo-2.svg") : Qt.resolvedUrl("../assets/logo.svg")

                onVisibleChanged: visible ? yumiAnimation.start() : yumiAnimation.stop()

                Timer {
                    id: yumiAnimation
                    triggeredOnStart: true
                    repeat: true
                    interval: 80

                    onTriggered: {
                        yumiFrame += 1;
                        if (yumiFrame > 24) yumiFrame = 0;
                    }
                }
            }

            CenteredLabel {
                text: i18n.tr("Welcome to %1!").arg("UBports")
                textSize: Label.XLarge
            }

            DefaultLabel {
                text: i18n.tr("Change the future. Innovate and dream again.")
                textSize: Label.Large
            }

            DefaultLabel {
                text: i18n.tr("A private and innovative experience is made possible by you, as one of us. Regain control of choice and freedom — on your device and with your personal data.")
            }

            Spacer {}

            UbuntuShape {
                width: units.gu(10)
                height: width
                anchors.horizontalCenter: parent.horizontalCenter
                aspect: UbuntuShape.Flat

                image: Image {
                    source: Qt.resolvedUrl("../assets/join.svg")
                    sourceSize.width: parent.width
                    sourceSize.height: parent.height
                }
            }

            DefaultLabel {
                text: i18n.tr("Be more than a consumer — help out.")
                textSize: Label.Large
            }

            DefaultLabel {
                //TRANSLATORS: Please ensure URLs are correct, otherwise the HTML fucks up.
                text: i18n.tr("Actually change what you don't like. Be frustrated and fix limitations. <a href='https://github.com/ubports/ubports-touch'>Report bugs and send feature requests</a>. We are already this far along.")
            }

            Spacer {}

            UbuntuShape {
                width: units.gu(10)
                height: width
                anchors.horizontalCenter: parent.horizontalCenter
                aspect: UbuntuShape.Flat

                image: Image {
                    source: Qt.resolvedUrl("../assets/donate.svg")
                    sourceSize.width: parent.width
                    sourceSize.height: parent.height
                }
            }

            DefaultLabel {
                text: i18n.tr("Pay what's fair")
                textSize: Label.Large
            }

            DefaultLabel {
                //TRANSLATORS: Please ensure the URLs are correct (and exist!), otherwise the HTML fucks up.
                text: i18n.tr("Community volunteers ensured no up-front costs and openness. Something to consider with a <a href='https://liberapay.com/ubports'>Liberapay</a>, or (as a worse option) one-time <a href='https://paypal.me/ubports'>PayPal</a>, or monthly <a href='https://patreon.com/ubports'>Patreon</a> donation. This help is all the more appreciated. You feel great about your fair contribution and everyone is excited to participate. In return, you are not promised, but get more than fair service.")
            }

            Spacer {}

            UbuntuShape {
                width: units.gu(10)
                height: width
                anchors.horizontalCenter: parent.horizontalCenter
                aspect: UbuntuShape.Flat

                source: Image {
                    source: Qt.resolvedUrl("../assets/notification.svg")
                    sourceSize.width: parent.width
                    sourceSize.height: parent.height
                }
            }

            DefaultLabel {
                text: i18n.tr("Get in")
                textSize: Label.Large
            }

            DefaultLabel {
                //TRANSLATORS: Please ensure URLs are correct (and exist!), otherwise the HTML fucks up.
                text: i18n.tr("We are in this together. You get the latest development info, related news and much more. Join the Matrix room <b>#ubports.matrix.org</b>, the <a href='https://t.me/ubports'>Telegram Supergroup</a>, the <a href='https://forums.ubports.com/'>forums</a> and <a href='https://ubports.com/'>the e-mail newsletter you can tailor to your needs</a>.")
            }

            DefaultLabel {
                //TRANSLATORS: Please ensure URLs are correct, otherwise the HTML fucks up.
                text: i18n.tr("Made possible by <a href='https://ubports.com/sponsors'>our awesome sponsors, donors and patrons</a>. Thank you.")
            }

            Spacer {}
        }
    }
}
